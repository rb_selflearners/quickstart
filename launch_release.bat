@echo off
setlocal EnableDelayedExpansion
set arch=x64
if not exist bin\unigine_project_!arch!d.exe (
	set arch=x86
) else (
	set PATH=bin\x64;!PATH!;
) 
start bin\unigine_project_%arch%.exe  -video_app auto -video_vsync 0 -video_refresh 0 -video_mode 1 -video_resizable 1 -video_fullscreen 0 -video_debug 0 -video_gamma 1.000000 -sound_app auto -data_path "../data/"  -engine_config "../data/unigine_project/unigine.cfg" -extern_plugin "Collada" -console_command "config_readonly 1 && world_load \"unigine_project/unigine_project\""