#include "AppSystemLogic.h"
// System logic, it exists during the application life cycle.
// These methods are called right after corresponding system script's (UnigineScript) methods.

AppSystemLogic::AppSystemLogic() {
	
}

AppSystemLogic::~AppSystemLogic() {
	
}

int AppSystemLogic::init() {
	// Write here code to be called on engine initialization.
	
	return 1;
}

// start of the main loop
int AppSystemLogic::update() {
	// Write here code to be called before updating each render frame.
	
	return 1;
}

int AppSystemLogic::render() {
	// Write here code to be called before rendering each render frame.
	
	return 1;
}
// end of the main loop

int AppSystemLogic::shutdown() {
	// Write here code to be called on engine shutdown.
	
	return 1;
}

int AppSystemLogic::destroy() {
	// Write here code to be called when the video mode is changed or the application is restarted (i.e. video_restart is called). It is used to reinitialize the graphics context.
	
	return 1;
}
