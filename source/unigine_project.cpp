#include <UnigineEngine.h>

#include "AppSystemLogic.h"
#include "AppWorldLogic.h"
#include "AppEditorLogic.h"

#ifdef _WIN32
	int wmain(int argc,wchar_t *argv[]) {
#else
	int main(int argc,char *argv[]) {
#endif
	
	Unigine::EnginePtr engine(UNIGINE_VERSION,argc,argv);
	
	AppSystemLogic system_logic;
	AppWorldLogic world_logic;
	AppEditorLogic editor_logic;
	
	engine->main(&system_logic,&world_logic,&editor_logic);
	
	return 0;
}
