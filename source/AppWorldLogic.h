#ifndef __APP_WORLD_LOGIC_H__
#define __APP_WORLD_LOGIC_H__

#include <UnigineLogic.h>
#include <UnigineStreams.h>
#include <UnigineObjects.h>
#include <UnigineEditor.h> 
#include <UnigineGame.h> 

using namespace Unigine;

class AppWorldLogic : public Unigine::WorldLogic {
	
public:
	AppWorldLogic();
	virtual ~AppWorldLogic();
	
	virtual int init();
	
	virtual int update();
	virtual int render();
	virtual int flush();
	
	virtual int shutdown();
	virtual int destroy();
	
	virtual int save(const Unigine::StreamPtr &stream);
	virtual int restore(const Unigine::StreamPtr &stream);

private:
	Editor* editor_{};
	Vector<ObjectMeshDynamicPtr> objects_;

	PlayerSpectatorPtr player_;

	int AddMeshToScene(const char* fileName, const char* meshName, const char* materialName, const Math::Vec3& position);
	int InitObjects();

	int InitPlayer();

};

#endif // __APP_WORLD_LOGIC_H__
