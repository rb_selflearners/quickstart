#include "AppWorldLogic.h"
// World logic, it takes effect only when the world is loaded.
// These methods are called right after corresponding world script's (UnigineScript) methods.

AppWorldLogic::AppWorldLogic() = default;

AppWorldLogic::~AppWorldLogic() {
	
}

int AppWorldLogic::init() {
	// Write here code to be called on world initialization: initialize resources for your world scene during the world start.
	editor_ = Editor::get();

	InitObjects();
	InitPlayer();
	return 1;
}

// start of the main loop
int AppWorldLogic::update() {
	// Write here code to be called before updating each render frame: specify all graphics-related functions you want to be called every frame while your application executes.
	
	return 1;
}

int AppWorldLogic::render() {
	// The engine calls this function before rendering each render frame: correct behavior after the state of the node has been updated.
	
	return 1;
}

int AppWorldLogic::flush() {
	// Write here code to be called before updating each physics frame: control physics in your application and put non-rendering calculations.
	// The engine calls flush() with the fixed rate (60 times per second by default) regardless of the FPS value.
	// WARNING: do not create, delete or change transformations of nodes here, because rendering is already in progress.
	
	return 1;
}
// end of the main loop

int AppWorldLogic::shutdown() {
	// Write here code to be called on world shutdown: delete resources that were created during world script execution to avoid memory leaks.
	for (int i = 0; i < objects_.size(); ++i)
	{
		editor_->removeNode(objects_[i].get()->getNode());
	}
	objects_.clear();

	player_.clear();
	return 1;
}

int AppWorldLogic::destroy() {
	// Write here code to be called when the video mode is changed or the application is restarted (i.e. video_restart is called). It is used to reinitialize the graphics context.
	
	return 1;
}

int AppWorldLogic::save(const Unigine::StreamPtr &stream) {
	// Write here code to be called when the world is saving its state (i.e. state_save is called): save custom user data to a file.
	
	UNIGINE_UNUSED(stream);
	return 1;
}

int AppWorldLogic::restore(const Unigine::StreamPtr &stream) {
	// Write here code to be called when the world is restoring its state (i.e. state_restore is called): restore custom user data to a file here.
	
	UNIGINE_UNUSED(stream);
	return 1;
}

int AppWorldLogic::AddMeshToScene(const char * fileName, const char * meshName, const char * materialName, const Math::Vec3& position)
{
	MeshPtr mesh = Mesh::create();
	if ( fileName )
	{
		if ( !mesh->load(fileName))
		{
			Log::error("\nError opening mesh file : %s!\n", fileName);
			mesh.clear();
			return 0;
		}
	} else
	{
		mesh->addBoxSurface( "box_surface", Math::vec3(0.5f));
	}
	ObjectMeshDynamicPtr omd = ObjectMeshDynamic::create(mesh);

	omd->setMaterial(materialName, "*");
	omd->setName(meshName);
	omd->setWorldPosition(position);

	omd->release();
	editor_->addNode(omd->getNode());

	objects_.append(omd);
	Log::message("-> Object %s added to the scene.\n", meshName);
	mesh.clear();

	return 1;
}

int AppWorldLogic::InitObjects()
{
	int index = 0;
	for (int x = 0; x < 2; ++x)
	{
		for (int y = 0; y < 2; ++y)
		{
			AddMeshToScene(nullptr, String::format("my_meshdynamic_%d", index).get(),
				"mesh_base", Math::Vec3( x, y, 1.0f));
			++index;
		}
	}
	Log::warning("Objects generated Ok!\n\n");
	return 1;
}

int AppWorldLogic::InitPlayer()
{
	player_ = PlayerSpectator::create();
	player_->setFov(90.0f);
	player_->setZNear(0.001f);
	player_->setZFar(10000.0f);
	
	player_->setPosition(Math::Vec3(3.0f));
	player_->setDirection(Math::vec3(-1.0f), Math::vec3(0.0f, 0.0f, -1.0f));

	Game::get()->setPlayer(player_->getPlayer());
	player_->release();

	Log::warning("Player initialization OK!");
	return 1;
}
