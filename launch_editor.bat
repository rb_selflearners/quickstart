@echo off
setlocal EnableDelayedExpansion
if not exist bin\editor_x64.exe (
	echo "Editor application not found, please reconfigure project"
) else (
	start bin\editor_x64.exe  -video_app auto -video_vsync 0 -video_refresh 0 -video_mode 1 -video_resizable 1 -video_fullscreen 0 -video_debug 0 -video_gamma 1.000000 -sound_app auto -data_path "../data/"  -engine_config "../data/unigine_project/unigine.cfg" -extern_plugin "Collada" -console_command "config_readonly 0 && world_load \"unigine_project/unigine_project\""
)